const contacts = [
  {
    "_id": "id0",
    "name": "Oleta Level",
    "phone_number": "+442032960159",
    "address": "10 London Wall, London EC2M 6SA, UK"
  }, {
    "_id": "id1",
    "name": "Maida Harju",
    "phone_number": "+442032960899",
    "address": "Woodside House, 94 Tockholes Rd, Darwen BB3 1LL, UK"
  }, {
    "_id": "id2",
    "name": "Lia Pigford",
    "phone_number": "+442032960182",
    "address": "23 Westmorland Cl, Darwen BB3 2TQ, UK"
  }, {
    "_id": "id3",
    "name": "Ghislaine Darden",
    "phone_number": "+442032960427",
    "address": "20-24 Knowlesly Rd, Darwen BB3 2NE, UK"
  }, {
    "_id": "id4",
    "name": "Jana Spitler",
    "phone_number": "+442032960370",
    "address": "4 St Lucia Cl, Darwen BB3 0SJ, UK"
  }, {
    "_id": "id5",
    "name": "Dolly Detweiler",
    "phone_number": "+442032960977",
    "address": "18 Johnson Rd, Darwen BB3, UK"
  }, {
    "_id": "id6",
    "name": "Stanley Vanderhoof",
    "phone_number": "+442032960000",
    "address": "17 Anchor Ave, Darwen BB3 0AZ, UK"
  }, {
    "_id": "id7",
    "name": "Adan Milian",
    "phone_number": "+442032960011",
    "address": "20 Ellerbeck Rd, Darwen BB3 3EX, UK"
  }, {
    "_id": "id8",
    "name": "Marivel Molina",
    "phone_number": "+442032960013",
    "address": "Tockholes Rd, Darwen BB3, UK"
  }, {
    "_id": "id9",
    "name": "Kris Everett",
    "phone_number": "+442032960012",
    "address": "Pinewood, Tockholes Rd, Darwen BB3 1JY, UK"
  } 
]

module.exports = { contacts }
