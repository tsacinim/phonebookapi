const MongoClient = require('mongodb').MongoClient;
const { users } = require('./data/users');
const { contacts } = require('./data/contacts');
const dbUrl = 'mongodb://localhost:27017';
const dbName = 'phonebook';

let db;

const loadDB = async () => {
    if (db) {
      return db;
    }
    try {
      const client = await MongoClient.connect(
        `${dbUrl}/${dbName}`,  
        { useNewUrlParser: true } 
      );
      db = client.db(dbName);
    } catch (err) {
      console.log(err)
    }
    return db;
};

const seedDB = async () => {
  const db = await loadDB();
  const usersCol = db.collection('users')
  const count = await usersCol.estimatedDocumentCount();
  if (!count) {
    await usersCol.insertMany(users)
    console.log(`Inserted ${users.length} test users`);
  } 
  const contactsCol = db.collection('contacts')
  const ccount = await contactsCol.estimatedDocumentCount();
  if (!ccount) {
    await contactsCol.insertMany(contacts)
    console.log(`Inserted ${contacts.length} test contacts`);
  }
}

module.exports = { loadDB, seedDB };