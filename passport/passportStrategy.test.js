const { tryLogin } = require('./passportStrategy.js');

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
}

describe('[Check Authorization]', () => {
  it('should return a 200 when body contains the right credentials', async () => {
    const mockRequest = () => {
      return { 
        body: { name: 'test', password: 'test' }
      };
    }
    const req = mockRequest();
    const res = mockResponse();
    await tryLogin(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({
      message: "OK",
      token: expect.any(String)
    });
  });
  it('should return a 401 when body contains wrong credentials', async () => {
    const mockRequest = () => {
      return { 
        body: { name: 'test', password: 'wrong' }
      };
    }
    const req = mockRequest();
    const res = mockResponse();
    await tryLogin(req, res);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({
      message: "password is incorrect"
    });
    expect(res.json).toMatchSnapshot()
  });
});