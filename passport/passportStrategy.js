const jwt = require('jsonwebtoken');
const passportJWT = require("passport-jwt");
const { loadDB } = require('../db/db');  

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'secretOrKey';

const strategy = new JwtStrategy(jwtOptions, async (jwt_payload, next) => {
  try {
    const db = await loadDB();
    const user = await db.collection('users').findOne({_id: jwt_payload._id})  
    if (user) {
      next(null, user);
    } else {
      next(null, false);
    }
  } catch (error) {
    console.log(error)  
    next(null, false)  
  }
});

const tryLogin = async (req, res) => {
  if(req.body.name && req.body.password) {
    var {name, password}  = req.body;
  }
  const db = await loadDB();
  const user = await db.collection('users')
    .findOne({name: name})
  if( !user ){
    res.status(401).json({message:"user NOT found"});
  }

  if(user.password === req.body.password) {
    const payload = {_id: user._id};
    const token = jwt.sign(payload, jwtOptions.secretOrKey);
    res.status(200).json({message: "OK", token: token});
  } else {
    res.status(401).json({message:"password is incorrect"});
  }
}

module.exports = {strategy, tryLogin}