const express = require('express');
const passport = require('passport');
const bodyParser = require("body-parser");
const { seedDB } = require('./db/db');  
const { strategy, tryLogin } = require('./passport/passportStrategy');
const { getList, getContact, createContact, 
  updateContact, deleteContact } = require('./routes/routes');
const errorhandler = require('errorhandler')

passport.use(strategy);

const app = express();
app.use(passport.initialize());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json())

app.get("/", (req, res) => {
  res.json({message: "Phonebook API server is running"});
});

app.post("/login", tryLogin);

app.get("/contacts", 
  passport.authenticate('jwt', { session: false }), 
  getList
);

app.get("/contacts/:id", 
  passport.authenticate('jwt', { session: false }), 
  getContact
);

app.post("/contacts", 
  passport.authenticate('jwt', { session: false }), 
  createContact
);

app.put("/contacts/:id", 
  passport.authenticate('jwt', { session: false }), 
  updateContact
);

app.delete("/contacts/:id", 
  passport.authenticate('jwt', { session: false }), 
  deleteContact
);

app.use(errorhandler())

const server = app.listen(3000, async () => {
  console.log("Express app listening on: localhost:3000");
  await seedDB();
});
 
module.exports = { app, server }