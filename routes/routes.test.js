const { getList, getContact, createContact, 
  updateContact, deleteContact } = require('./routes');
const { loadDB } = require('../db/db');

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
}

describe('[Phonebook API]', () => {
  it('should allow to READ a list of phonebook contacts', async () => {
    const mockRequest = () => ({})
    const req = mockRequest();
    const res = mockResponse();
    await getList(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toMatchSnapshot()
  });
  it('should allow to READ a phonebook contact by ID', async () => {
    const mockRequest = () => ({
      params: { id: 'id0' }
    })
    const req = mockRequest();
    const res = mockResponse();
    await getContact(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toMatchSnapshot()
  });
  it('should allow to CREATE a new a phonebook contact', async () => {
    const db = await loadDB();
    await db.collection('contacts').deleteOne({_id: 'id10'})
    const mockRequest = () => ({
      body: {"contact":
        {
          "_id": "id10",
          "name": "Stefan Minica",
          "phone_number": "+12345678910",
          "address": "71 Malukkenstraat, 1095 AX, Amsterdam, NL"
        }
      }
    })
    const req = mockRequest();
    const res = mockResponse();
    await createContact(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toMatchSnapshot()
  });
  it('should allow to UPDATE an existing phonebook contact', async () => {
    const mockRequest = () => ({
      params: { id: 'id1'},
      body: {"contact":
        {
          "name": "Stefan Minica",
          "phone_number": "+12345678910",
          "address": "71 Malukkenstraat, 1095 AX, Amsterdam, NL"
        }
      }
    })
    const req = mockRequest();
    const res = mockResponse();
    await updateContact(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toMatchSnapshot()
  });
  it('should allow to REMOVE an existing phonebook contact', async () => {
    const mockRequest = () => ({
      params: { id: 'id10'}
    })
    const req = mockRequest();
    const res = mockResponse();
    await deleteContact(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toMatchSnapshot()
  });
});

const {server} = require('../server')
server.close()