const { loadDB } = require('../db/db');  

const getList = async (req, res) => {
  try {
    const db = await loadDB();
    const contacts = await db.collection('contacts')
      .find({}).limit(20).toArray()
    res.status(200).json({data: contacts});      
  } catch (error) {
    res.status(500)
      .json({message: "internal server error"})            
  }
}

const getContact = async (req, res) => {
  try {
    const { id } = req.params;
    const db = await loadDB();
    const contact = await db.collection('contacts')
      .findOne({_id: id})
    if (contact) {
      res.status(200).json({data: contact});
    } else {
      res.status(404)
      .json({message: `User with _id: ${id} not found`})
    }      
  } catch (error) {
    res.status(500)
      .json({message: "internal server error"})      
  }
}

const createContact = async (req, res) => {
  try {
    const { contact } = req.body;
    const db = await loadDB();
    const id = contact._id;
    const exists = await db.collection('contacts')
      .findOne({_id: id })
    if (exists && exists._id === id) {
      res.status(409).json({
        message: `A contact with _id: ${id} already exists`
      });  
    } else {
      const response = await db.collection('contacts')
        .insertOne(contact)
      res.status(201).json({_id: response.insertedId});      
    }
  } catch (error) {
    res.status(500)
      .json({message: "internal server error"})      
  }
}

const updateContact = async (req, res) => {
  try {
    const { id } = req.params;
    const { contact } = req.body; 
    const newId = contact._id   
    if (newId && id !== newId) {
      res.status(409).json({
        message: `The _id field cannot be changed`
      });
    } else {
      const db = await loadDB();
      const response = await db.collection('contacts')
        .updateOne({_id: id.toString()}, {$set: contact} )
      res.status(200).json({data: response});  
    }
  } catch (error) {
    res.status(500)
      .json({message: "internal server error"})      
  }
}

const deleteContact = async (req, res) => {
  try {
    const { id } = req.params;
    const db = await loadDB();
    const response = await db.collection('contacts')
      .deleteOne({_id: id.toString()})
    res.status(200).json({data: response});      
  } catch (error) {
    res.status(500)
      .json({message: "internal server error"})      
  }
}

module.exports = {
  getList, getContact, createContact, updateContact, deleteContact
}