# Phonebook API

## Install dependencies

* Clone the repo `git clone https://gitlab.com/tsacinim/phonebookapi.git`
* If needed, install `node` (with `npm`) and `mongodb` (from their respective websites)
* Install needed dependencies `npm i` (in the root folder)

## Run the app

* Start the database: `mongod` or `npm run start:mongodb`
* Start the server: `node server` or `npm start`
* Start `Postman`; import `collection.json` and `environment.json` from `postman/` (in the root folder)
* Select the `PhonebookAPI` collection and environment
* Call the endpoints (you will need to call the `login` endpoint first)
* Alternatively, use `curl` or `wget`, etc.
* To run in dev mode (using `nodemon`): `npm run dev`

## Run unit tests (for routes and authentication)

* Start and run all tests once `npm run test` (the server should not be running)
* Run all tests in watch mode `npm run tdd`

## Run functional tests (at endpoints level)

* Start and run all tests once (using `newman`): `npm run test:api` (the server and DB should be started first)
* Functional tests can also be run from inside Postman: `Runner -> Collection Runner -> Select PhonebookAPI Collection -> Run PhonebookAPI Collection` (the server and DB should be running)  
